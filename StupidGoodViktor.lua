local UseProdiction = false
--[[
  Stupid Good Viktor by StupidGuy30
	==================================
	
	Change log:
	0.62:
		- Hopefully fixed issue with Prod

	0.61:
		- Fixed AA range in orbwalk code

	0.6:
		- W added to combo with toggle for number of enemies(I still don't like it)
		- Move To Mouse Implemented(toggle autoattack too)
		- Ult autotracks to nearest clump of enemies(toggle)
		- Added more Drawing Options
		- Removed solo ult option. Just use the slider
		- Removed E packet cast toggle. Now does it if you are VIP no matter what.
		- Prediction changed to % option

	0.54:
		- Fixed another error with Free Prediction
		- Cleaned up code (Pulled my prediction logic to their own functions)

	0.53:
		- Fixed error keeping Free users from using this script.

	0.52:
		- Fixed more errors in Prodiction Code
		- Tweaked prediction numbers for prodiction to fire more like VPrediction does

	0.51:
		- Fixed minor MEC code bug in prodiction code

	0.5:
		- Prodiction Support added
		- Fixed bug in MEC on ult
		- Fixed bug on casting ult with packets
		- Uses latency as delay now
	
	0.41:
		- Fixed bug with combo not casting laser when ult is up.
	
	0.4:
		- Renamed Stupid Good Viktor
		- Uses MEC and prediction on ult
		- Menu Revamped - For the future
		- Reworked a bit of Laser Code
		- Sureness on laser in combo is lowered.
		- Added toggles for packet casting of Q,E, and R
	
	0.3:
		-Fixed speed on laser in calcs
		-Fixed bug with laser vector against enemies on different sides of you
		-Added another Target Selector for Q+R as the E is too long range to matter for them
		-Other minor improvements in laser code
	
	0.2:
		- Fixed minor error in Two Target E code
		- Added check for range changes on E
	
	0.1:
		-Initial Release
		
	]]--

if myHero.charName ~= "Viktor" then return end

if VIP_USER then
	if UseProdiction then
		require "Prodiction"
	
		local Prod
		local EPro
		local PPro
	else
		require "VPrediction"
	
		local VP
	end
end

local ATTS -- AverageTimeToServer

local ESpeed = 700 / .9 -- Yes, RiotDecode shows 1200 as it's speed, but I've watched it numerous times on replays and it definitely moves the full 700 units in exactly .9 seconds, giving it a speed of about 777.78

local UltInProgress, UltTimer = false, 0 -- Used for Ult code

local PokeTarget, ComboTarget = nil, nil -- Used to draw targets

local LaserAngle, LaserAngleTimer = 0, 0 -- Used to make drawing the pokeline not skitter about
local PokeStart, PokeEnd, PokeHit, PokeTimer = nil, nil, 0, 0 -- Used to draw poke line

-- Used by orbwalking code. Credits to Manciuszz
local lastAttack, lastWindUpTime, lastAttackCD = 0, 0, 0
local lastAnimation

--[[
	OnLoad sets up your target prediction and sets up
	the menu.
	]]--
function OnLoad()
	ATTS = GetLatency()/2
	
	ESelector = TargetSelector(TARGET_LESS_CAST_PRIORITY, 1300, DAMAGE_MAGIC, false)
	ESelector.name = "Laser Target"
	QRSelector = TargetSelector(TARGET_LESS_CAST_PRIORITY, 800, DAMAGE_MAGIC, false)
	QRSelector.name = "Q and R Target"

	ViktorHelpConfig = scriptConfig("Viktor", "Viktor")
	ViktorHelpConfig:addTS(ESelector)
	ViktorHelpConfig:addTS(QRSelector)

	ViktorHelpConfig:addSubMenu("Hotkeys","Hotkeys")
		ViktorHelpConfig.Hotkeys:addParam("harrassbutton2", "Harrass with Q", SCRIPT_PARAM_ONKEYDOWN, false, 71) 
		ViktorHelpConfig.Hotkeys:addParam("harrassbutton", "Poke with E", SCRIPT_PARAM_ONKEYDOWN, false, 86)
		ViktorHelpConfig.Hotkeys:addParam("combobutton", "Combo", SCRIPT_PARAM_ONKEYDOWN, false, 32)

	ViktorHelpConfig:addSubMenu("Poke(E Options)","Poke")
		if VIP_USER then
			ViktorHelpConfig.Poke:addParam("randomangle", "Random E Angle Harrass (vs Single Target)", SCRIPT_PARAM_ONOFF, true)
			ViktorHelpConfig.Poke:addParam("anglelimit", "Rotate Poke +- X�", SCRIPT_PARAM_SLICE, 15, 0, 45, 0)
		end
		ViktorHelpConfig.Poke:addParam("override", "Target Override", SCRIPT_PARAM_ONOFF, false)

	ViktorHelpConfig:addSubMenu("Ability Options","Combo")
		ViktorHelpConfig.Combo:addParam("comboult", "Use R in Combo", SCRIPT_PARAM_ONOFF, false)
		ViktorHelpConfig.Combo:addParam("ultables", "Minimum targets to ult", SCRIPT_PARAM_SLICE, 3, 1, 5, 0)
		ViktorHelpConfig.Combo:addParam("moveUlt", "Auto Move Ult", SCRIPT_PARAM_ONOFF, true)
		ViktorHelpConfig.Combo:addParam("comboW", "Use W in Combo", SCRIPT_PARAM_ONOFF, false)
		ViktorHelpConfig.Combo:addParam("wAble", "Minimum targets to W", SCRIPT_PARAM_SLICE, 3, 1, 5, 0)
		if VIP_USER then
			ViktorHelpConfig.Combo:addParam("packetQ", "Packet Cast Q", SCRIPT_PARAM_ONOFF, false)
			ViktorHelpConfig.Combo:addParam("packetW", "Packet Cast W", SCRIPT_PARAM_ONOFF, false)
			ViktorHelpConfig.Combo:addParam("packetR", "Packet Cast R", SCRIPT_PARAM_ONOFF, true)
		end

	ViktorHelpConfig:addSubMenu("Prediction","Prediction")
		if VIP_USER then
			if UseProdiction then
				Prod = ProdictManager.GetInstance()
				ViktorHelpConfig.Prediction:addParam("predict", "Prodiction", SCRIPT_PARAM_ONOFF, true)
			
				EPro = Prod:AddProdictionObject(_E, 700, ESpeed, 0.25, 90)
				PPro = Prod:AddProdictionObject(_Q, 10000, math.huge, 0.25, 0)
			else
				VP = VPrediction()
				ViktorHelpConfig.Prediction:addParam("predict", "VPredict", SCRIPT_PARAM_ONOFF, true)
			end
		end
			ViktorHelpConfig.Prediction:addParam("sureness", "Poke Sureness", SCRIPT_PARAM_SLICE, 75, 50, 100, 0 )
			ViktorHelpConfig.Prediction:addParam("cSure", "Combo Sureness", SCRIPT_PARAM_SLICE, 65, 50, 100, 0)
	
	ViktorHelpConfig:addSubMenu("OrbWalker", "OrbWalk")
		ViktorHelpConfig.OrbWalk:addParam("moveHarass", "Move to Mouse(Harass)", SCRIPT_PARAM_ONOFF, false)
		ViktorHelpConfig.OrbWalk:addParam("attackHarass", "Force Attack(Harass)", SCRIPT_PARAM_ONOFF, true)
		ViktorHelpConfig.OrbWalk:addParam("moveCombo", "Move to Mouse(Combo)", SCRIPT_PARAM_ONOFF, false)
		ViktorHelpConfig.OrbWalk:addParam("attackCombo", "Force Attack(Combo)", SCRIPT_PARAM_ONOFF, false)

	ViktorHelpConfig:addSubMenu("Drawing", "Drawing")
		ViktorHelpConfig.Drawing:addParam("drawPokeCircle", "Draw Poke Range", SCRIPT_PARAM_ONOFF, true)
		ViktorHelpConfig.Drawing:addParam("drawComboCircle", "Draw Combo Range", SCRIPT_PARAM_ONOFF, true)
		ViktorHelpConfig.Drawing:addParam("drawPokeTarget", "Draw Poke Target", SCRIPT_PARAM_ONOFF, true)
		ViktorHelpConfig.Drawing:addParam("drawComboTarget", "Draw Combo Target", SCRIPT_PARAM_ONOFF, true)
		ViktorHelpConfig.Drawing:addParam("drawPokeLine", "Draw Considered Poke", SCRIPT_PARAM_ONOFF, false)
		ViktorHelpConfig.Drawing:addParam("pokeLineUpdate", "Update Poke Line in ms", SCRIPT_PARAM_SLICE, 100, 0, 500, 0)

	ViktorHelpConfig:addParam("Version", "Version", SCRIPT_PARAM_INFO, "0.62")
	PrintChat("Stupid Good Viktor by StupidGuy30 v0.62 loaded successfully")
end

--[[
	CastQ casts Q. Checks if you packet or normal cast it.

	target: Unit. Unit to cast Q on
	]]--
function CastQ(target)
	if VIP_USER and ViktorHelpConfig.Combo.packetQ then
		Packet("S_CAST", {spellId = _Q, targetNetworkId = target.networkID}):send()
	else
		CastSpell(_Q, target)
	end
end

--[[
	CastE casts E. Uses packets by default.

	StartPosition: Vector. Position to start the laser
	EndPosition: Vector. Position to end the laser
	]]--
function CastE(StartPosition, EndPosition)
	if VIP_USER and EndPosition ~= nil then
		Packet("S_CAST", {spellId = _E, toX = EndPosition.x, toY = EndPosition.z, fromX = StartPosition.x, fromY = StartPosition.z}):send()
	else
		CastSpell(_E, StartPosition.x, StartPosition.z)
	end
end

--[[
	CastW casts W. Checks if you use packets or just normal cast it.

	Position: Vector. Position to cast ult on.
	]]--
function CastW(Position)
	if VIP_USER and ViktorHelpConfig.Combo.packetW then
		Packet('S_CAST', { spellId = _W, fromX = Position.x, fromY = Position.z}):send()
	else
		CastSpell(_W, Position.x, Position.z)
	end
end

--[[
	CastR casts R. Checks if you use packets or just normal cast it.

	Position: Vector. Position to cast ult on.
	]]--
function CastR(Position)
	if VIP_USER and ViktorHelpConfig.Combo.packetR then
		Packet('S_CAST', { spellId = _R, fromX = Position.x, fromY = Position.z}):send()
	else
		CastSpell(_R, Position.x, Position.z)
	end
end

--[[
	AdjustHitChance gives a value to adjust your hitchance
	by when using VPrediction or free prediction.

	Start: Vector. Position to compare range to
	Position: Vector. Position that you are firing at
	MaxRange: Number. Maximum range the skill can be fired
	MaxAdjust: Number. The maximum adjustment to hit.

	Returns: Number. The value between 0 and MaxAdjust to
			adjust your hitchance by
	]]--
function AdjustHitChance(Start, Position, MaxRange, MaxAdjust)
	local dSqr = GetDistanceSqr(Start, Position)
	local Adjust = 0
	local CurAdjPer = 9
	
	for i=20, 1, -1 do
		local AdjRange = MaxRange - MaxRange * (20-i)/ 20
		if dSqr < AdjRange * AdjRange then
			Adjust = Adjust + (MaxAdjust * CurAdjPer / 100)
			if i % 5 ~= 1 then
				CurAdjPer = CurAdjPer - .5
			end
		else
			return Adjust
		end
	end
	return Adjust
end

--[[
	ePredict predicts the endpoint for a laser vs a
	single target

	From: Vector. Position the laser starts at
	Target: Unit. Unit to fire at.
	MyPos: Vector. The position you will be at when firing

	Returns: Vector. Endpoint for laser
		Number. HitChance of laser 0-100
		Vector. Starting point of laser(for free prediction)
	]]--
function ePredict(From, Target, MyPos)
	local CastPosition, HitChance
	local StartPosition = From

	if MyPos == nil then
		MyPos = Vector(myHero.x, 0, myHero.z)
	end
	
	if VIP_USER and ViktorHelpConfig.Prediction.predict then
		if UseProdiction then
			EPro.Spell.delay = ATTS + .02
			CastPosition, timey, HitChance = EPro:GetPrediction(Target, StartPosition)

			if CastPosition == nil then
				CastPosition = Vector(target.x, 0, target.z)
			end
			
			if HitChance == nil then
				HitChance = 0
			elseif HitChance == 100 or HitChance == 1 then
				HitChance = 100
			elseif HitChance >= .70 then
				HitChance = 75
			elseif HitChance >= .45 then
				HitChance = 50
			else
				HitChance = 25
			end
			if HitChance ~= 100 then
				HitChance = HitChance + AdjustHitChance(StartPosition, Target, 700, 25)
			end
		else
			CastPosition, HitChance, Position = VP:GetLineCastPosition(Target, ATTS + .02, 0, 700, ESpeed, StartPosition)
			if HitChance == 0 then
				HitChance = 25
			elseif HitChance == 1 then
				HitChance = 50
			elseif HitChance == 2 then
				HitChance = 75
			else
				HitChance = 100
			end
			if HitChance ~= 100 then
				HitChance = HitChance + AdjustHitChance(StartPosition, Target, 700, 25)
			end
		end
	else
		local tp = TargetPrediction(1250, 1390, ATTS + .02, 0)
		CastPosition = tp:GetPrediction(Target)
		
		if CastPosition == nil then
			HitChance = 0
			CastPosition = Vector(Target.x, 0, Target.z)
		else
			HitChance = 50
		end
		
		local CastDirection = (CastPosition-MyPos):normalized()
		local dsqr = GetDistanceSqr(CastPosition,MyPos)
		if dsqr >= 302500 then
			StartPosition = MyPos + CastDirection*550
		else
			StartPosition = MyPos + CastDirection*(math.sqrt(dsqr)-10)
		end
		HitChance = HitChance + AdjustHitChance(StartPosition, Target, 700, 50)
	end
	return CastPosition, HitChance, StartPosition
end

--[[
	pPredict predicts the position a unit will be at after
	a delay.

	Target: Unit. Unit to predict
	Delay: Number. Amount of miliseconds before prediction

	Returns: Vector. Position after delay
		Number. HitChance of laser 0-100. Does not adjust
			for free/vp as it doesn't have a point of
			reference.
	]]--
function pPredict(Target, Delay)
	local Position, HitChance

	if VIP_USER and ViktorHelpConfig.Prediction.predict then
		if UseProdiction then
			PPro.Spell.delay = Delay
			Position, timey, HitChance = PPro:GetPrediction(Target)
			
			if Position == nil then
				Position = Vector(Target.x, 0, Target.z)
			end
			
			if HitChance == nil then
				HitChance = 0
			elseif HitChance == 100 or HitChance == 1 then
				HitChance = 100
			elseif HitChance >= .70 then
				HitChance = 75
			elseif HitChance >= .45 then
				HitChance = 50
			else
				HitChance = 25
			end
		else
			Position, HitChance = VP:GetPredictedPos(Target, Delay)
			if HitChance == 0 then
				HitChance = 25
			elseif HitChance == 1 then
				HitChance = 50
			elseif HitChance == 2 then
				HitChance = 75
			else
				HitChance = 100
			end
		end
	else
		Position = GetPredictionPos(Target, Delay)

		if Position == nil then
			Position = Vector(Target.x, 0, Target.z)
			HitChance = 0
		else
			HitChance = 50
		end
	end
	return Position, HitChance
end

--[[
	CalculateLaserPath calculates if a path can be established
	between two targets.

	priorityTargetInfo: Table. Two values, target and distance to
		target.
	targetToTest: Unit. Second target.
	myPos: Vector. Position the hero will be at when firing.

	Returns: Vector. Starting point of laser
		Vector. Endpoint for laser
		Number. HitChance of laser 0-150
	]]--
function CalculateLaserPath(priorityTargetInfo, targetToTest, MyPos)
	if targetToTest.networkID == priorityTargetInfo.target.networkID then
		return nil, nil, 0
	end
	
	local DistanceBetweenTargets = GetDistance(priorityTargetInfo.target, targetToTest)
	if DistanceBetweenTargets > 750 then -- Range is 700, with some prediction, 750 should be ok
		return nil, nil, 0
	end
	
	local DistanceToTestTarget = GetDistance(MyPos,targetToTest)
	if DistanceToTestTarget > 1300 then -- Max Distance is 1250, so should be k
		return nil, nil, 0
	end
	
	local TargetClose, TargetFar, DistanceClose, DistanceFar
	if (priorityTargetInfo.distance >= DistanceToTestTarget) then -- Second target is closer
		TargetClose = targetToTest
		DistanceClose = DistanceToTestTarget
		TargetFar = priorityTargetInfo.target
		DistanceFar = priorityTargetInfo.distance
	else -- first target is closer
		TargetFar = targetToTest
		DistanceFar = DistanceToTestTarget
		TargetClose = priorityTargetInfo.target
		DistanceClose = priorityTargetInfo.distance
	end
	
	if DistanceClose > 550 and DistanceFar < DistanceBetweenTargets then -- Can't make the line
		return nil, nil, 0
	end
	
	-- Ok gotta make some predictions. First Time to hit the targets, then where they'll be
	-- TODO: Figure out time script takes to run
	local TimeClose, TimeFar = 0, 0
	if DistanceClose <= 550 then
		TimeClose = ATTS + .02
	else
		TimeClose = ATTS + .02 + (DistanceClose - 550) / ESpeed
	end
	TimeFar = TimeClose + DistanceBetweenTargets / ESpeed
	
	if TimeFar > .92 + ATTS then -- Skill takes .9 seconds to land and latency calc
		return nil, nil, 0
	end
	
	-- Predict positions
	local PredictClose, PredictFar, HitClose, HitFar

	PredictClose, HitClose = pPredict(TargetClose, TimeClose)
	PredictFar, HitFar = pPredict(TargetFar, TimeFar)
	
	-- Check Predicted distances to see if they are in line
	PredictDistanceSqrBetweenTargets = GetDistanceSqr(PredictClose, PredictFar)
	if PredictDistanceSqrBetweenTargets > 490000 then
		return nil, nil, 0
	end
	
	PredictCloseDistanceSqr = GetDistanceSqr(MyPos, PredictClose)
	if PredictCloseDistanceSqr > 1562500 then
		return nil, nil, 0
	end
	
	PredictFarDistanceSqr = GetDistanceSqr(MyPos, PredictFar)
	if PredictFarDistanceSqr > 1562500 then
		return nil, nil, 0
	end
	
	
	if PredictCloseDistanceSqr < 302500 then -- The closer target is within minimum range
		return PredictClose, PredictFar, HitClose+HitFar
	end
	
	if PredictFarDistanceSqr < PredictDistanceSqrBetweenTargets then -- won't work
		return nil, nil, 0
	end
	
	-- OK gotta find out if this line intersects Viktor's cast range!
	local LocalClose = Vector(PredictClose.x - MyPos.x, 0, PredictClose.z - MyPos.z)
	local LocalFar = Vector(PredictFar.x - MyPos.x, 0, PredictFar.z - MyPos.z)
	
	-- calculate a, b, c for quadratic
	local a = ((LocalFar.x - LocalClose.x) * (LocalFar.x - LocalClose.x) + (LocalClose.z - LocalFar.z) * (LocalClose.z - LocalFar.z))
	local b = 2 * (LocalClose.x * (LocalFar.x + LocalClose.x) + LocalClose.z * (LocalFar.z - LocalClose.z))
	local c = LocalClose.x * LocalClose.x + LocalClose.z * LocalClose.z - 301950
	local u = 0
	
	-- Check if there is an intersection
	local checker = b * b - 4 * a * c
	if checker < 0 then -- there's no intersection
		return nil, nil, 0
	end
	
	local StartPosition
	if checker == 0 then -- We have a tangent
		u = (-1 * b) / (2 * a)
		StartPosition = Vector(MyPos.x + LocalClose.x + u * (LocalFar.x - LocalClose.x), 0, MyPos.z + LocalClose.z + u * (LocalFar.z - LocalClose.z))
	else -- We have a chord
		local sqrter = math.sqrt(checker)
		local u1 = (-b + sqrter) / (2 * a)
		local u2 = (-b - sqrter) / (2 * a)
		local point1 = Vector(MyPos.x + LocalClose.x + u1 * (LocalFar.x - LocalClose.x), 0, MyPos.z + LocalClose.z + u1 * (LocalFar.z - LocalClose.z))
		local point2 = Vector(MyPos.x + LocalClose.x + u2 * (LocalFar.x - LocalClose.x), 0, MyPos.z + LocalClose.z + u2 * (LocalFar.z - LocalClose.z))
		
		if GetDistanceSqr(point1,PredictClose) < GetDistanceSqr(point2, PredictClose) then
			StartPosition = point1
		else
			StartPosition = point2
		end
	end
	
	if GetDistanceSqr(StartPosition, PredictFar) < PredictDistanceSqrBetweenTargets then
		-- They are on opposite sides of the circle
		return nil, nil, 0
	end
	
	-- Gotta fix our HitClose and HitFar values if using VPrediction
	if not VIP_USER or not ViktorHelpConfig.Prediction.predict then
		HitClose = AdjustHitChance(StartPosition, TargetClose, 700, 50)
		HitFar = AdjustHitChance(StartPosition, TargetFar, 700, 50)
	elseif VIP_USER then
		HitClose = AdjustHitChance(StartPosition, TargetClose, 700, 25)
		HitFar = AdjustHitChance(StartPosition, TargetFar, 700, 25)
	end
	
	-- TODO: Check how many targets it will collide with
	
	return StartPosition, PredictFar, (HitClose + HitFar)/2 + 20
end

--[[
	ConsiderLaser considers all multitarget lasers against a single
	target laser and provides the ideal one.

	target: Unit. target to aim at.

	Returns: Vector. Starting point of laser
		Vector. Endpoint for laser
		Number. HitChance of laser 0-150
	]]--
function ConsiderLaser(target)
	if target == nil or myHero:CanUseSpell(_E) ~= READY then
		return nil, nil, 0
	end
	
	local BestStart, BestCast, BestHit = nil, nil, 0

	local StartPosition, CastPosition = nil , nil
	local HitChance = 0
	local myPredPos
		
	myPredPos = pPredict(myHero, ATTS + .02)
		
	local DSqr1 = GetDistanceSqr(myHero,target)
	local DSqr2 = GetDistanceSqr(myPredPos, target)
			
	local Distance, MyPos
			
	if DSqr1 >= DSqr2 then
		-- We're moving toward the enemy
		MyPos = Vector(myHero.x, 0, myHero.z)
		Distance = math.sqrt(DSqr1)
	else
		--We're moving away from the enemy
		MyPos = myPredPos
		Distance = math.sqrt(DSqr2)
	end
			
	if VIP_USER then
		targetTable = {}
		targetTable.distance = Distance
		targetTable.target = target
		for i, TargetToTest in ipairs(GetEnemyHeroes()) do
			StartPosition, CastPosition, HitChance = CalculateLaserPath(targetTable, TargetToTest, MyPos)
			if (HitChance > BestHit) then
				BestStart = StartPosition
				BestCast = CastPosition
				BestHit = HitChance
			end
		end
	end
		
	if Distance <= 550 then
		-- cast on target
		StartPosition = Vector(target.x, target.y, target.z)
		CastPosition, HitChance, StartPosition = ePredict(StartPosition, target, MyPos)
	else		
		-- Find the Directional Vector
		local TargetPos = Vector(target.x, target.y, target.z)
		local CastDirection = (TargetPos-MyPos)
		
		-- Check if we rotate the Vector!
		if VIP_USER and ViktorHelpConfig.Poke.randomangle and (CastDirection.x ~= 0 and CastDirection.z ~= 0) then
			-- Convert Vector to Radian
			local RadianDirection = math.atan2(CastDirection.z, CastDirection.x)
			
			-- Rotate it!
			RadianDirection = RadianDirection+LaserAngle
			
			-- Convert it back
			CastDirection.x = math.cos(RadianDirection)
			CastDirection.z = math.sin(RadianDirection)
		else
			CastDirection:normalize()
		end
		
		-- Cast at Max Range
		StartPosition = Vector(MyPos.x + CastDirection.x*550, 0, MyPos.z + CastDirection.z*550)
		
		CastPosition, HitChance, StartPosition = ePredict(StartPosition, target, MyPos)
	end

	if BestHit > HitChance then
		StartPosition = BestStart
		CastPosition = BestCast
		HitChance = BestHit
	end
	
	if GetDistanceSqr(CastPosition, StartPosition) > 490000 then
		return nil, nil, 0
	end
	
	-- Make sure the end position is always 700 units away
	CastPosition = StartPosition + (CastPosition-StartPosition):normalized() * 700
	
	return StartPosition, CastPosition, HitChance
end

--[[
	FireLaser considers the laser then fires if it's hitchance
	is of the correct value.

	target: Unit. Target to aim at.
	Sureness: Number. Threshold of hitchance to fire.
	]]--
function FireLaser(Target, Sureness)
	local StartPosition, CastPosition, HitChance = 	ConsiderLaser(Target)
	if HitChance >= Sureness then
		CastE(StartPosition,CastPosition)
	end
end

--[[
	FindBestCircle uses MEC to find the best circle for Ult's and
	W's. It is based on Honda's MEC code for Orianna. I could not
	for the life of me get GetMEC to work.

	target: Unit. Target that needs to be hit.
	range: Number. Range of the spell.
	radius: Number. Radius of the spell.

	Returns: Vector. Position to fire circle ability.
		Number. Number of targets it will hit.
	]]--
function FindBestCircle(target, range, radius)
	local points = {}
	
	local rgDsqr = (range + radius) * (range + radius)
	local diaDsqr = (radius * 2) * (radius * 2)

	local Position
	Position = pPredict(target, ATTS + .02)

	table.insert(points,Position)
	
	for i, enemy in ipairs(GetEnemyHeroes()) do
		if enemy.networkID ~= target.networkID and not enemy.dead and GetDistanceSqr(enemy) <= rgDsqr and GetDistanceSqr(target,enemy) < diaDsqr then
			Position = pPredict(enemy, ATTS + .02)
			table.insert(points, Position)
		end
	end
	
	while true do
		local MECObject = MEC(points)
		local OurCircle = MECObject:Compute()
		
		if OurCircle.radius <= radius then
			return OurCircle.center, #points
		end
		
		local Dist = -1
		local MyPoint = points[1]
		local index = 0
		
		for i=2, #points, 1 do
			local DistToTest = GetDistanceSqr(points[i], MyPoint)
			if DistToTest >= Dist then
				Dist = DistToTest
				index = i
			end
		end
		if index > 0 then
			table.remove(points, index)
		else
			return points[1], 1
		end
	end
end

--[[
	MoveUlt moves your Ult towards the biggest batch of baddies
	near you.
	]]--
function MoveUlt() -- MOVE ZIG
	local CurrentPos, CurrentNum = nil, 0
	for i, enemy in ipairs(GetEnemyHeroes) do
		if not enemy.dead then
			local TestPos, TestNum = FindBestCircle(enemy, 700, 250)
			local TestDistance = GetDistance(TestPos)
			
			if TestDistance < 700 then
				TestNum = TestNum + TestDistance/525
			end
			
			if TestNum > CurrentNum then
				CurrentPos = TestPos
				CurrentNum = TestNum
			end
		end
	end
	
	if CurrentPos == nil then
		CurrentPos = myHero
	end
	
	CastR(CurrentPos)
end

--[[
	Combo performs your combo in order of Q->R(optional)->W(optional)->E
	]]--
function Combo() 
	Distance = GetDistanceSqr(ComboTarget)
	if myHero:CanUseSpell(_Q) == READY and Distance <= 360000 then
		CastQ(ComboTarget)
		return
	end
		
	if ViktorHelpConfig.Combo.comboult and not UltInProgress then
		if myHero:CanUseSpell(_R) == READY and Distance <= 490000 then
			local positionToUlt, numberOfEnemies = FindBestCircle(ComboTarget, 700, 250)
			
			if numberOfEnemies >= ViktorHelpConfig.Combo.ultables then
				CastR(positionToUlt)
				return
			end
		end
	end

	if ViktorHelpConfig.Combo.comboW and myHero:CanUseSpell(_W) and Distance <= 390625 then
		local positionToW, numberOfEnemies = FindBestCircle(ComboTarget, 625, 300)
		
		if numberOfEnemies >= ViktorHelpConfig.Combo.wAble then
			CastW(positionToW)
			return
		end
	end
	
	FireLaser(ComboTarget, ViktorHelpConfig.Prediction.cSure)
end

--[[
	OnTick is where the magic happens. Updates TargetSelectors, updates ATTS
	finds you targets for poke/ult for drawing, get's lines ready for drawing on
	poke. Checks if you are combo/harass/poking. Moves to mouse. Automatically moves
	Ult. Basically the meat of the script.
	]]--
function OnTick()
	ESelector:update()
	QRSelector:update()
	
	ATTS = (ATTS + GetLatency() / 2) / 2
	
	local currentTick = GetTickCount()

	if not myHero.dead then
		PokeTarget = nil
		if ViktorHelpConfig.Poke.override then
			PokeTarget = GetTarget()
		end

		if PokeTarget == nil or not IsEnemy(PokeTarget) then
			PokeTarget = ESelector.target
		end

		if PokeTarget ~= nil then
			if ViktorHelpConfig.Hotkeys.harrassbutton or (ViktorHelpConfig.Drawing.drawPokeLine and currentTick - ViktorHelpConfig.Drawing.pokeLineUpdate > PokeTimer) then
				if VIP_USER and ViktorHelpConfig.Poke.randomangle and (ViktorHelpConfig.Hotkeys.harassbutton or currentTick-1000 > LaserAngleTimer) then
					LaserAngle = (math.random()-.5)*ViktorHelpConfig.Poke.anglelimit*2*math.pi/180
					LaserAngleTimer = currentTick
				end

				PokeStart, PokeEnd, PokeHit = ConsiderLaser(PokeTarget)
				PokeTimer = currentTick
			end

			if ViktorHelpConfig.Hotkeys.harrassbutton and PokeHit >= ViktorHelpConfig.Prediction.sureness and heroCanMove() then
				CastE(PokeStart, PokeEnd)
			end
		end
		
		ComboTarget = QRSelector.target
		
		if ViktorHelpConfig.Hotkeys.combobutton or ViktorHelpConfig.Hotkeys.harrassbutton2 then
			if ViktorHelpConfig.OrbWalk.moveHarass then
				local target = nil
				if ViktorHelpConfig.OrbWalk.attackHarass then
					target = ComboTarget
				end
				OrbWalking(target)
			end
		end
		
		if ComboTarget ~= nil and heroCanMove() then
			if ViktorHelpConfig.Hotkeys.harrassbutton2 then
				if myHero:CanUseSpell(_Q) == READY and GetDistanceSqr(ComboTarget) <= 360000 then
					CastQ(ComboTarget)
				end
			end
			if ViktorHelpConfig.Hotkeys.combobutton then
				if ViktorHelpConfig.OrbWalk.moveCombo then
					local target = nil
					if ViktorHelpConfig.OrbWalk.attackCombo then
						target = ComboTarget
					end
					OrbWalking(target)
				end
				Combo()
			end
		end
		
		if UltInProgress then
			if myHero:CanUseSpell(_R) ~= READY then
				UltInProgress = false
			elseif ViktorHelpConfig.Combo.moveUlt and currentTick - 25 < UltTimer then
				MoveUlt()
				UltTimer = currentTick
			end
		end
	end
end

--[[
	IsEnemy checks if a unit is an enemy player.
	
	Unit: Unit. Unit to check
	
	Returns: Bool. True if enemy player, false if not
	]]--
function IsEnemy(Unit)
	for i, enemy in ipairs(GetEnemyHeroes()) do
		if Unit.networkID == enemy.networkID then
			return true
		end
	end
	return false
end

--[[
	OnProcessSpell is used to check if Ult was cast and
	by the orbwalker.
	
	object: Object. Object that cast the spell
	spell: Spell. Spell that is being cast
	]]--
function OnProcessSpell(object, spell)
	if object.name == player.name then
		if spell.name == "ViktorChaosStorm" then
			UltInProgress = true
		end
		if spell.name:lower():find("attack") then
			lastAttack = GetTickCount() - ATTS
			lastWindUpTime = spell.windUpTime*1000
			lastAttackCD = spell.animationTime*1000
		end
	end
end

--[[
	OnDraw draws our stuff.
	]]--
function OnDraw()
	if not myHero.dead then
		if ViktorHelpConfig.Drawing.drawPokeCircle then
			DrawCircle(myHero.x, myHero.y, myHero.z, 1250, 0xFF80FF00)
		end

		if ViktorHelpConfig.Drawing.drawComboCircle then
			DrawCircle(myHero.x, myHero.y, myHero.z, 600, 0xFFFF8000)
		end

		if ViktorHelpConfig.Drawing.drawPokeTarget and PokeTarget ~= nil then
			DrawCircle(PokeTarget.x, PokeTarget.y, PokeTarget.z, 100, 0xFFFF0000)
		end

		if ViktorHelpConfig.Drawing.drawComboTarget and ComboTarget ~= nil then
			DrawCircle(ComboTarget.x, ComboTarget.y, ComboTarget.z, 75, 0xFF00FF00)
		end

		if ViktorHelpConfig.Drawing.drawPokeLine and PokeStart ~= nil and PokeTarget ~= nil then
			local start = WorldToScreen(D3DXVECTOR3(PokeStart.x, myHero.y, PokeStart.z))
			local stop = WorldToScreen(D3DXVECTOR3(PokeEnd.x, myHero.y, PokeEnd.z))
			if PokeHit < ViktorHelpConfig.Prediction.sureness then
				color = 0xFFFF0000
			else
				if PokeHit < 100 then
					color = 0xFF0000FF
				else
					color = 0xFF00FF00
				end
			end
			DrawLine(start.x, start.y, stop.x, stop.y, 3, color)	
		end
	end
end
	
-- Credits to Manciuszz for orbwalker code
-- Feel bad taking code, but there's only so many ways
-- to skin a cat.
--[[
	OrbWalking orbwalks a target
	
	target: Unit to orbwalk
	]]--
function OrbWalking(target)
	if TimeToAttack() and target ~= nil and GetDistanceSqr(target) <= 360000 then
		myHero:Attack(target)
	elseif heroCanMove() then
		moveToCursor()
	end
end

--[[
	TimeToAttack checks if you can attack again.
	
	Returns: Bool. True if you can, false if not
	]]--
function TimeToAttack()
	return (GetTickCount() + GetLatency()/2 > lastAttack + lastAttackCD)
end

--[[
	heroCanMove checks if your attack animation has played
	long enough to move after an attack.
	
	Returns: Bool. True if you can, false if not
	]]--
function heroCanMove()
	return (GetTickCount() + GetLatency()/2 > lastAttack + lastWindUpTime + 20)
end

--[[
	moveToCursor moves your hero towards your cursor.
	]]--
function moveToCursor()
	if GetDistance(mousePos) then
		local moveToPos = myHero + (Vector(mousePos) - myHero):normalized()*300
		myHero:MoveTo(moveToPos.x, moveToPos.z)
	end        
end
 
function OnAnimation(unit,animationName)
	if unit.isMe and lastAnimation ~= animationName then lastAnimation = animationName end
end
-- End of OrbWalking Code. Again hate stealing code, but
-- there's only so many ways to swing a weapon.
