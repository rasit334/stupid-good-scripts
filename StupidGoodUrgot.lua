local UseProdiction = false
--[[
  Stupid Good Urgot by StupidGuy30
	==================================

	Change Log:
	0.2:
		- Now prioritizes your target over any other targets.
		- Fixed Delays
		- Improved Combo/Teamfight button
		- Tweaked hit chance adjustment
		- Added Orbwalking
	
	0.1:
		- Initial release

	]]--
if VIP_USER then
	if UseProdiction then
		require "Prodiction"
		require "Collision"

		local Prod, EPro, QPro, PPro
	else
		require "VPrediction"

		local VP
	end
end

local tm
local ATTS -- AverageTimeToServer, used for delay calcs
local UrgotConfig

-- Used by orbwalking code. Credits to Manciuszz
local lastAttack, lastWindUpTime, lastAttackCD = 0, 0, 0
local lastAnimation

local PoisonTable = {} -- Used to store potential Q targets

local qTp, eTp -- Free prediction variables

local eLandedTime = 0 -- Timer for Shield in combo

if myHero.charName ~= "Urgot" then return end

--[[
	OnLoad is used to setup our script. It configures our
	menu and jump starts our prediction.
	]]--
function OnLoad()
	ATTS = GetLatency()/2

	tm = TargetSelector(TARGET_LESS_CAST_PRIORITY, 1000, DAMAGE_PHYSICAL)
	tm.name = "Termination Mode"

	UrgotConfig = scriptConfig("Stupid Good Urgot", "SGU")
	UrgotConfig:addTS(tm)

	UrgotConfig:addSubMenu("Hotkey Options", "Hotkey")
		UrgotConfig.Hotkey:addParam("qHarrass", "Harass with Q", SCRIPT_PARAM_ONKEYDOWN, false, 71)
		UrgotConfig.Hotkey:addParam("ePoison", "Poison with E", SCRIPT_PARAM_ONKEYDOWN, false, 86)
		UrgotConfig.Hotkey:addParam("Combo", "Use Combo", SCRIPT_PARAM_ONKEYDOWN, false, 32)

	UrgotConfig:addSubMenu("Ability Options", "Ability")
		UrgotConfig.Ability:addParam("qMana", "Q Harass Min Mana %", SCRIPT_PARAM_SLICE, 40, 0, 100, 0)
		UrgotConfig.Ability:addParam("wCombo", "Use W on poison in Combo", SCRIPT_PARAM_ONOFF, false)
		UrgotConfig.Ability:addParam("qPacket", "Packet Cast Q", SCRIPT_PARAM_ONOFF, false)
		UrgotConfig.Ability:addParam("wPacket", "Packet Cast W", SCRIPT_PARAM_ONOFF, true)
		UrgotConfig.Ability:addParam("ePacket", "Packet Cast E", SCRIPT_PARAM_ONOFF, false)
	
	UrgotConfig:addSubMenu("Orbwalking Options", "OrbWalk")
		UrgotConfig.OrbWalk:addParam("qOrb", "OrbWalk Harass", SCRIPT_PARAM_ONOFF, false)
		UrgotConfig.OrbWalk:addParam("cOrb", "OrbWalk Combo", SCRIPT_PARAM_ONOFF, false)
		UrgotConfig.OrbWalk:addParam("orbTarget", "Force OrbWalk target to TS/Target", SCRIPT_PARAM_ONOFF, false)
	
	UrgotConfig:addSubMenu("Prediction Options", "Predict")
		if VIP_USER then
			if UseProdiction then
				Prod = ProdictManager.GetInstance()
				UrgotConfig.Predict:addParam("pTrigger", "Prodiction", SCRIPT_PARAM_ONOFF, true)

				QPro = Prod:AddProdictionObject(_Q, 1000, 1600, 0.175, 20)
				EPro = Prod:AddProdictionObject(_E, 900, 1750, 0.250, 250)
			else
				VP = VPrediction()
				UrgotConfig.Predict:addParam("pTrigger", "VPredict", SCRIPT_PARAM_ONOFF, true)
			end
		end
		UrgotConfig.Predict:addParam("Sure", "Accuracy %", SCRIPT_PARAM_SLICE, 75, 50, 100, 0)

	UrgotConfig:addSubMenu("Drawing Options", "Draw")
		UrgotConfig.Draw:addParam("rCircles", "Range Circles", SCRIPT_PARAM_ONOFF, true)

	UrgotConfig:addParam("Version", "Version", SCRIPT_PARAM_INFO, "0.2")
	
	qTp = TargetPrediction(1000, 1600, ATTS + .175, 20)
	eTp = TargetPrediction(900, 1750, ATTS + .25, 250)
	
	PrintChat("Stupid Good Urgot by StupidGuy30 v0.2 loaded successfully")
end

--[[
	CastQ casts Q, unsurprisingly. It takes in account whether
	or not you use packet casting.

	Position: A Vector. The position to fire Q at.
	]]--
function CastQ(Position)
	if VIP_USER and UrgotConfig.Ability.qPacket then
		Packet("S_CAST", {spellId = _Q, fromX = Position.x, fromY = Position.z}):send()
	else
		CastSpell(_Q, Position.x, Position.z)	
	end
end

--[[
	CastW casts W. Simple.
	]]--
function CastW()
	if VIP_USER and UrgotConfig.Ability.wPacket then
		Packet("S_CAST", {spellId = _W}):send()
	else
		CastSpell(_W)
	end
end

--[[
	CastE casts E. Checks VIP and config if you can
	packet cast.

	Position: A Vector. The position to fire E at.
	]]--
function CastE(Position)
	if VIP_USER and UrgotConfig.Ability.ePacket then
		Packet("S_CAST", {spellId = _E, fromX = Position.x, fromY = Position.z}):send()
	else
		CastSpell(_E, Position.x, Position.z)
	end
end

--[[
	AdjustHitChance gives a value to adjust your hitchance
	by when using VPrediction or free prediction.

	Position: Vector. Position that you are firing at
	MaxRange: Number. Maximum range the skill can be fired
	MaxAdjust: Number. The maximum adjustment to hit.

	Returns: Number. The value between 0 and MaxAdjust to
			adjust your hitchance by
	]]--
function AdjustHitChance(Position, MaxRange, MaxAdjust)
	local dSqr = GetDistanceSqr(Position)
	local Adjust = 0
	local CurAdjPer = 9
	
	for i=20, 1, -1 do
		local AdjRange = MaxRange - MaxRange * (20-i)/ 20
		if dSqr < AdjRange * AdjRange then
			Adjust = Adjust + (MaxAdjust * CurAdjPer / 100)
			if i % 5 ~= 1 then
				CurAdjPer = CurAdjPer - .5
			end
		else
			return Adjust
		end
	end
	return Adjust
end

--[[
	PredictQ is a function that uses your selected prediction
	to place where you should fire your Q at.

	Target: Unit. Target to fire at.

	Returns: Vector. Position to fire at.
		Number. Chance to hit target.
	]]--
function PredictQ(Target)
	local CastPosition, HitChance
	if VIP_USER and UrgotConfig.Predict.pTrigger then
		if UseProdiction then
			QPro.Spell.delay = ATTS + .175
			CastPosition, timeywimey, HitChance = QPro:GetPrediction(Target)
			if CastPosition == nil or CastPosition == Target then
				CastPosition = Vector(Target.x, 0, Target.z)
			end

			if HitChance == nil then
				HitChance = 0
			elseif HitChance ~= 100 then
				HitChance = HitChance*100
			end

			local col = Collision(1000, 1600, ATTS + .175, 20)
			if col:GetMinionCollision(CastPosition, myHero) then
				HitChance = -1
			end
		else
			CastPosition, HitChance, Position = VP:GetLineCastPosition(Target, ATTS + .175, 20, 1000, 1600, myHero, true)
			if HitChance == 0 then
				HitChance = 25
			elseif HitChance == 1 then
				HitChance = 50
			elseif HitChance == 2 then
				HitChance = 75
			elseif HitChance ~= -1 then
				HitChance = 100
			end
			if HitChance ~= -1 then
				HitChance = HitChance + AdjustHitChance(Target, 1000, 25)
			end
		end
	else
		qTp.delay = ATTS + .175
		CastPosition, minionCollision = qTp:GetPrediction(Target)
		
		if minionCollision then
			HitChance = -1
		else
			HitChance = 50
		end
		
		if CastPosition == nil then
			CastPosition = Vector(Target.x, 0, Target.z)
			HitChance = -2
		end

		if HitChance ~= -1 then
			HitChance = HitChance + AdjustHitChance(Target, 1000, 50)
		end
	end
	return CastPosition, HitChance
end

--[[
	PredictE is a function that uses your selected prediction
	to place where you should fire your E at.

	Target: Unit. Target to fire at.

	Returns: Vector. Position to fire at.
		Number. Chance to hit target.
	]]--
function PredictE(Target)
	local CastPosition, HitChance
	if VIP_USER and UrgotConfig.Predict.pTrigger then
		if UseProdiction then
			EPro.Spell.delay = ATTS + .25
			CastPosition, timeywimey, HitChance = EPro:GetPrediction(Target)
			if CastPosition == nil then
				CastPosition = Vector(Target.x, 0, Target.z)
			end

			if HitChance == nil then
				HitChance = 0
			elseif HitChance ~= 100 then
				HitChance = HitChance*100
			end
		else
			CastPosition, HitChance, Position = VP:GetCircularCastPosition(Target, ATTS + .25, 250, 900, 1750, myHero, false)
			if HitChance == 0 then
				HitChance = 25
			elseif HitChance == 1 then
				HitChance = 50
			elseif HitChance == 2 then
				HitChance = 75
			elseif HitChance ~= -1 then
				HitChance = 100
			end
			HitChance = HitChance + AdjustHitChance(Target, 900, 25)
		end
	else
		eTp.delay = ATTS + .25
		CastPosition = eTp:GetPrediction(Target)
		
		if CastPosition == nil then
			CastPosition = Vector(Target.x, 0, Target.z)
			HitChance = 0
		end

		HitChance = 50

		if HitChance ~= -1 then
			HitChance = HitChance + AdjustHitChance(Target, 900, 50)
		end
	end
	return CastPosition, HitChance
end

--[[
	ArrangeQPriority arranges your MMATarget, your Target, the
	TargetSelector's target, and every poisoned unit in a list of
	who to Q over who.

	Returns: Table. Table in order of who to Q at.
	]]--
function ArrangeQPriority()
	local targetList = {}
	
	if tm.target ~= nil then
		table.insert(targetList, tm.target)
	end
	
	if _G.MMA_Loaded and _G.MMA_Target ~= nil then
		table.insert(targetList, _G.MMA_Target)
	end
	
	for i = 1, #PoisonTable, 1 do
		local dSqr = GetDistanceSqr(PoisonTable[i].person)
		if dSqr <= 1440000 and ValidTarget(PoisonTable[i].person) then
			table.insert(targetList, PoisonTable[i].person)
		end
	end
	
	local targetOutput = {}
	
	local unit = GetTarget()
	if unit ~= nil and IsEnemy(unit) then
		table.insert(targetOutput, unit)
	end
	
	while #targetList > 0 do
		local currentIndex = 0
		local lowestHP = math.huge
		for i = 1, #targetList, 1 do
			local armorMultiplier = 1
			if targetList[i].armor >= 0 then
				armorMultiplier = 100 / (100 + targetList[i].armor)
			else
				armorMultiplier = 2 - 100 / (100 - targetList[i].armor)
			end
			local hp = targetList[i].health / armorMultiplier
			if hp < lowestHP and not targetList[i].dead then
				currentIndex = i
				lowestHP = hp
			end
		end
		table.insert(targetOutput, targetList[currentIndex])
		table.remove(targetList, currentIndex)
	end
	return targetOutput
end

--[[
	AcidHunter fires Acid Hunter. It does it based on the table returned
	by ArrangeQPriority.
	
	Returns: Bool. True if cast, false if not
	]]--
function AcidHunter()
	local QTargets = ArrangeQPriority()
	
	for i = 1, #QTargets, 1 do
		if IsUnitPoisoned(QTargets[i]) then
			CastQ(QTargets[i])
			return true
		end
		
		local CastPosition, HitChance = PredictQ(QTargets[i])
		if HitChance >= UrgotConfig.Predict.Sure then
			CastQ(CastPosition)
			return true
		end
	end
	return false
end

--[[
	NoxianCorrosiveCharge fires a NoxianCorrosiveCharge
	
	Returns: Bool. True if cast, false if not
	]]--
function NoxianCorrosiveCharge()
	local Target = GetTarget()
	
	if (Target == nil or not IsEnemy(Target)) or GetDistanceSqr(Target) < 810000 then
		Target = tm.target
	end
	
	local CastPosition, HitChance = PredictE(tm.target)
	if HitChance >= UrgotConfig.Predict.Sure then
		CastE(CastPosition)
		return true
	end
	return false
end

--[[
	Combo sets the orders of spells fired when we hold our Combo
	button.
	]]--
function Combo()
	if myHero:CanUseSpell(_Q) == READY then
		if AcidHunter() then return end
	end
	
	if myHero:CanUseSpell(_E) == READY and tm.target ~= nil then
		if NoxianCorrosiveCharge() then return end
	end
	
	if myHero:CanUseSpell(_W) == READY and UrgotConfig.Ability.wCombo and eLandedTime ~= 0 then
		CastW()
	end
end

--[[
	OnTick is where the magic happens. It updates our TargetSelector, 
	maintains the poison list, keeps track of time since last E, averages
	your ping variable, and checks keys and handles code.
	]]--
function OnTick()
	tm:update()
	MaintainPoison()
	
	if eLandedTime ~= 0 then
		if GetTickCount() - eLandedTime >= 2000 then
			eLandedTime = 0
		end
	end
	
	ATTS = (ATTS + GetLatency() / 2) / 2
	
	if not myHero.dead and ((not _G.MMA_Loaded and heroCanMove()) or (_G.MMA_Loaded and _G.MMA_AbleToMove)) then
		local OrbTarget = GetOrbWalkTarget()
		if UrgotConfig.Hotkey.Combo then
			if UrgotConfig.OrbWalk.cOrb then
				OrbWalking(OrbTarget)
			end
			Combo()
		end
		
		if UrgotConfig.Hotkey.qHarrass and myHero:CanUseSpell(_Q) == READY and 100 * myHero.mana / myHero.maxMana >= UrgotConfig.Ability.qMana then
			if UrgotConfig.OrbWalk.qOrb then
				OrbWalking(OrbTarget)
			end
			AcidHunter()
		end
		
		if tm.target ~= nil then
			if UrgotConfig.Hotkey.ePoison and myHero:CanUseSpell(_E) == READY then
				NoxianCorrosiveCharge()
			end
		end
	end
end

--[[
	IsEnemy checks if a unit is an enemy player.
	
	Unit: Unit. Unit to check
	
	Returns: Bool. True if enemy player, false if not
	]]--
function IsEnemy(Unit)
	for i, enemy in ipairs(GetEnemyHeroes()) do
		if Unit.networkID == enemy.networkID then
			return true
		end
	end
	return false
end

--[[
	OnApplyParticle callback is used to update your PoisonTable
	with newly poisoned enemies.
	]]--
function OnApplyParticle(unit,particle)
  if particle.name == "UrgotCorrosiveDebuff_buf.troy" then
		if IsEnemy(unit) then
			table.insert(PoisonTable, {person = unit, caught = GetTickCount()})
			eLandedTime = GetTickCount()
		end
	end
end

--[[
	MaintainPoison iterates the poisoned target table and removes
	entries that are expired.
	]]--
function MaintainPoison()
	PointsToRemove = {}
	CurrentTick = GetTickCount()
	for i=1, #PoisonTable, 1 do
		if PoisonTable[i].caught + 5000 <= CurrentTick then
			table.insert(PointsToRemove, i)
		end
	end
	
	for i = #PointsToRemove, 1, -1 do
		table.remove(PoisonTable, i)
	end
end

--[[
	IsUnitPoisoned checks if a target is currently poisoned
	by Urgot's E.

	Unit: Unit. Unit to check if it's poisoned

	Returns: Bool. True if poisoned. False if not.
	]]--
function IsUnitPoisoned(Unit)
	for i = 1, #PoisonTable, 1 do
		if PoisonTable[i].person.networkID == Unit.networkID then
			return true
		end
	end
	return false
end

--[[
	OnDraw draws our circles.
	]]--
function OnDraw()
	if not myHero.dead and UrgotConfig.Draw.rCircles then
		DrawCircle(myHero.x, myHero.y, myHero.z, 1000, 0xFF80FF00)
		DrawCircle(myHero.x, myHero.y, myHero.z, 900, 0xFFFF8000)
	end
end

--[[
	GetOrbWalkTarget finds a suitable target in range of you to orbwalk.
	
	Returns: Unit. Suitable target within range.
	]]--
function GetOrbWalkTarget()
	selectedTarget = GetTarget()
	if selectedTarget ~= nil and IsEnemy(selectedTarget) and GetDistanceSqr(selectedTarget) <= 180625 then
		return selectedTarget
	end
	
	if tm.target ~= nil and GetDistanceSqr(tm.target) <= 180625 then
		return tm.target
	end
	
	if UrgotConfig.OrbWalk.orbTarget then
		return nil
	end
	
	possibleTarget = nil
	possibleHealth = math.huge
	for i, enemy in ipairs(GetEnemyHeroes()) do
		if tm.target ~= enemy and selectedTarget ~= enemy and not enemy.dead and GetDistanceSqr(enemy) <= 180625 then
			local armorMultiplier = 1
			if enemy.armor >= 0 then
				armorMultiplier = 100 / (100 + enemy.armor)
			else
				armorMultiplier = 2 - 100 / (100 - enemy.armor)
			end
			local hp = enemy.health / armorMultiplier
			if hp < possibleHealth then
				possibleTarget = i
				possibleHealth = hp
			end
		end
	end
	return possibleTarget
end

-- Credits to Manciuszz for orbwalker code
-- Feel bad taking code, but there's only so many ways
-- to skin a cat.

--[[
	OnProcessSpell is used to tell when Urgot is attacking.
	
	unit: Unit. Unit casting the spell
	spell: Spell. Spell being cast
	]]--
function OnProcessSpell(unit, spell)
	if unit.networkID == myHero.networkID then
		if spell.name:lower():find("attack") then
			lastAttack = GetTickCount() - ATTS
			lastWindUpTime = spell.windUpTime*1000
			lastAttackCD = spell.animationTime*1000
		end
	end
end

--[[
	OrbWalking orbwalks a target
	
	target: Unit to orbwalk
	]]--
function OrbWalking(target)
	if TimeToAttack() and target ~= nil and GetDistanceSqr(target) <= 180625 then
		myHero:Attack(target)
	elseif heroCanMove() then
		moveToCursor()
	end
end

--[[
	TimeToAttack checks if you can attack again.
	
	Returns: Bool. True if you can, false if not
	]]--
function TimeToAttack()
	return (GetTickCount() + GetLatency()/2 > lastAttack + lastAttackCD)
end

--[[
	heroCanMove checks if your attack animation has played
	long enough to move after an attack.
	
	Returns: Bool. True if you can, false if not
	]]--
function heroCanMove()
	return (GetTickCount() + GetLatency()/2 > lastAttack + lastWindUpTime + 20)
end

--[[
	moveToCursor moves your hero towards your cursor.
	]]--
function moveToCursor()
	if GetDistance(mousePos) then
		local moveToPos = myHero + (Vector(mousePos) - myHero):normalized()*300
		myHero:MoveTo(moveToPos.x, moveToPos.z)
	end        
end
 
function OnAnimation(unit,animationName)
	if unit.isMe and lastAnimation ~= animationName then lastAnimation = animationName end
end
-- End of OrbWalking Code. Again hate stealing code, but
-- there's only so many ways to swing a weapon.